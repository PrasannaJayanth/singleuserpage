<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Details & Register Form</title>
    <link rel="stylesheet" href="./assests/css/bootstrap.min.css">
   
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="col-md-12 mt-5 mb-3">
                    <label for="formGroupEmail" class="form-label">Name</label>
                    <input type="text" class="form-control " name="fullname" id="fullname" placeholder="Enter your Name">
                </div>
                <div class="col-md-12 mb-3">
                    <label for="formGroupEmail" class="form-label">Email</label>
                    <input type="text" class="form-control " name="emailid" id="emailid" placeholder="Enter your email">
                </div>
                <div class="col-md-12 mb-3">
                    <label for="formGroupMobile" class="form-label">Mobile Number</label>
                    <input type="number" class="form-control " name="mobilenumber" id="mobilenumber" placeholder="Enter your mobile number">
                </div>
                <div class="col-md-12 mb-3">
                    <label for="formGroupPassword" class="form-label">Password</label>
                    <input type="password" class="form-control " name="password" id="password" placeholder="Enter your pasword">
                </div>
                <div class="col-md-12 mb-3">
                    <button type="submit" class="btn btn-primary" name="submitbutton" id="submitbutton">Submit</button>
                </div>
            </div>
            <div class="col-lg-8">

                <div class="table-responsive">
                    <table class="table mt-5 ">
                        <thead>
                            <tr>
                                <th scope="col">Sl.no</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Mobile</th>
                                <th scope="col">Password</th>
                                <th scope="col">Operation</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="">
                                <th scope="row">1</th>
                                <td>Siva</td>
                                <td>siva123@gmail.com</td>
                                <td>6758943263</td>
                                <td>123@#siva</td>
                                <td>
                                    <button type="button" class="btn btn-primary"><a class="text-light text-decoration-none" href="#">Update</a></button>
                                    <button type="button" class="btn btn-danger ms-1"><a class="text-light text-decoration-none" href="#">Delete</a></button>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-6">

            </div>
        </div>
    </div>


    <script src="./assests/js/bootstrap.min.js"></script>
</body>

</html>